package com.example.hospital.model;

//import java.time.LocalDateTime;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment {
    private Doctor author;
    private Date date;
    private String message;

    public Comment(Doctor author, Date date, String message) {
        this.author = author;
        this.date = date;
        this.message = message;
    }

    public Doctor getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public JSONObject toJSON() throws JSONException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String strDate = dateFormat.format(date);

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("date", strDate);
        jsonObject.put("doctorID", author.getID());
        jsonObject.put("message", message);

        return jsonObject;
    }

    public static Comment fromJSON(JSONObject jsonObject)
            throws JSONException, ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(jsonObject.getString("date"));

        int id = jsonObject.getInt("doctorID");
        Doctor doctor = Repo.doctors.get(id - 1);
        String message = jsonObject.getString("message");

        return new Comment(doctor, date, message);
    }
}
