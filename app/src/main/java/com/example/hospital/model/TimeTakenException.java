package com.example.hospital.model;

public class TimeTakenException extends Exception {
    public TimeTakenException() {
        super("time is already taken");
    }
}
