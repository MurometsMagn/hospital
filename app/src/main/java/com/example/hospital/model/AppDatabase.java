package com.example.hospital.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {User.class, Speciality.class, Admin.class,
        Doc.class, Pat.class}, version = 1, exportSchema = false) //room
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();

    public abstract SpecialityDao specialityDao();
}
