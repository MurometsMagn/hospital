package com.example.hospital.model;

/*
Приложение "больница".
Б сод набор врачей и админов. Пац-ты записываюца на прием к врачам, для этого вводят врача
( специализацию или конкретного врача) и время приема.
Адм-ры управляют списками записанных пациентов и формируют очередь.
Врачи принимают пац-в по сфоримированной очереди, составляют диагноз, выписывают назначение и рецепты.
Могут делать комментарии к пациентам (которые пац-етниам не показывают).
Пациенты могут видеть диагноз, назначение и рецепты.
Приложение открывается тремя видами авторизованных пользователей: администраторы, врачи, пациенты.
 */

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

//андроид подд-ет только sqlite
//room - это ОРМ библиотека для андроид (sqlite)
@Entity //класс является сущностью ОРМ (room)
public class User {
    @PrimaryKey(autoGenerate = true) //(room)
    public int ID;
    @ColumnInfo(name = "login") //сопоставляемые со столбцами в таблице (room)
    public String login = "";
    @ColumnInfo(name = "password")
    public String password = "";
    @ColumnInfo(name = "firstName")
    public String firstName = "";
    @ColumnInfo(name = "middleName")
    public String middleName = "";
    @ColumnInfo(name = "lastName")
    public String lastName = "";

    public User() {
    }

    @Ignore
    public User(String login, String password, String firstName,
                String middleName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getID() {
        return ID;
    }

    public String getFullName() {
        return (lastName + " " + firstName + " " + middleName);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", ID);
        jsonObject.put("login", login);
        jsonObject.put("password", password);
        jsonObject.put("firstName", firstName);
        jsonObject.put("middleName", middleName);
        jsonObject.put("lastName", lastName);

        return jsonObject;
    }
}

/*
дописать toJSON в User
написать toJSON  в Patient, Doctor, Administrator с использованием toJSON из User
 */