package com.example.hospital.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Speciality {
    //поля публичные, потому что это модель
    @PrimaryKey
    public int id;
    @ColumnInfo(name = "name")
    public String name;
}
