package com.example.hospital.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/*модели - это обычные классы java beans
репозитории - это интерфейсы
AppDatabase - это абстрактный класс
 */

@Entity
public class Admin {
    @PrimaryKey
    public int id;
    @ColumnInfo(name = "userId")
    public int userId;
}
