package com.example.hospital.model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class PatUser {
    @Embedded //встроенный /*все столбцы указанной сущности располагаются в той же таблице*/
    public User user;
    @Relation(
            parentColumn = "ID",
            entityColumn = "userId"
    )
    public Pat pat;
}
