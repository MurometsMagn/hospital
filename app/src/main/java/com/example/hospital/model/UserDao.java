package com.example.hospital.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.security.cert.LDAPCertStoreParameters;
import java.util.List;

//User, UserDao, AppDatabase - модель, репозиторий, база данных - относятся к room

@Dao //(room)
public interface UserDao {
    @Query("SELECT * FROM user") //(room)
    LiveData<List<User>> getAll();

    //@Transaction
    @Query("SELECT * FROM user INNER JOIN admin ON user.ID=admin.userId")
    LiveData<List<AdminUser>> getAllAdmins();

    @Query("SELECT * FROM user INNER JOIN doc ON user.ID=doc.userId")
    LiveData<List<DocUser>> getAllDoctors();

    @Query("SELECT * FROM user INNER JOIN pat ON user.ID=pat.userId")
    LiveData<List<PatUser>> getAllPatients();

    @Transaction
    @Query("SELECT * FROM user WHERE id = :userId")
    LiveData<AdminUser> getAdminUser(int userId);

    @Transaction
    @Query("SELECT * FROM user WHERE id = :userId")
    LiveData<DocUser> getDocUser(int userId);

    @Transaction
    @Query("SELECT * FROM user WHERE id = :userId")
    LiveData<PatUser> getPatUser(int userId);

    @Insert
    void add(Admin admin);

    @Insert
    void add(Pat patient);

    @Insert
    void add(Doc doctor);
}

/*13.04.21
реализовать модельный класс пациента
реализовать связующий класс PatientUser
добавить получение PatientUser в UserDao
 */
