package com.example.hospital.model;

//Адм-ры управляют списками записанных пациентов и формируют очередь.

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class Administrator extends User {
    public Administrator(int ID, String login, String password, String firstName, String middleName, String lastName) {
        super(login, password, firstName, middleName, lastName);
    }

    public void makeAppointment(Date date, Patient patient, Doctor doctor) throws TimeTakenException{
        Appointment appointment = new Appointment(date, patient);
        doctor.addAppointment(appointment);
    }

    public void cancelAppointment(Appointment appointment, Doctor doctor) {
        doctor.removeAppointment(appointment);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        return super.toJSON();
    }

    public static Administrator fromJSON(JSONObject jsonObject)
            throws JSONException {
        int id = jsonObject.getInt("id");
        String login = jsonObject.getString("login");
        String password = jsonObject.getString("password");
        String firstName = jsonObject.getString("firstName");
        String middleName = jsonObject.getString("middleName");
        String lastName = jsonObject.getString("lastName");

        return new Administrator(id, login, password, firstName, middleName, lastName);
    }
}

/*
админу: метод кенселАппойнтмент кот отм-т назн-е у заданного врача на заданное время
врачу добавить специальность
в репо добавить возм-сть получать список врачей заданной специальности
 */