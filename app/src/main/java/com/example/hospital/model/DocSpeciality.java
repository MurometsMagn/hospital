package com.example.hospital.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Relation;

public class DocSpeciality {
    @Embedded
    public Doc doctor;
    @Relation(
            parentColumn = "specialityId",
            entityColumn = "id"
    )
    Speciality speciality;

}
