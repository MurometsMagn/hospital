package com.example.hospital.model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class AdminUser { //один к одному
    @Embedded
    public User user;
    @Relation(
            parentColumn = "ID",
            entityColumn = "userId"
    )
    public Admin admin;
}
