package com.example.hospital.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Appointment {
    public final Date date;
    public final Patient patient;

    public Appointment(Date date, Patient patient) {
        this.date = date;
        this.patient = patient;
    }

    public JSONObject toJSON() throws JSONException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");  //HH - 24hr, kk - 24hr: 1-24
        String strDate = dateFormat.format(date);

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("date", strDate);
        jsonObject.put("patientID", patient.getID());

        return jsonObject;
    }

    public static Appointment fromJSON(JSONObject jsonObject)
            throws JSONException, ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormat.parse(jsonObject.getString("date"));

        int id = jsonObject.getInt("patientID");

        Patient patient = Repo.patients.get(id - 1);

        return new Appointment(date, patient);
    }
}

/*
Date date = Calendar.getInstance().getTime();
DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
String strDate = dateFormat.format(date);
 */