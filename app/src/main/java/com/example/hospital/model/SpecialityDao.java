package com.example.hospital.model;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SpecialityDao {
    @Query("SELECT * FROM speciality")
    List<Speciality> getAll();
}
