package com.example.hospital.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Pat {
    @PrimaryKey
    public int id;
    @ColumnInfo(name = "userId")
    public int userId;
}
