package com.example.hospital.model;

import android.content.Intent;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.hospital.UI.old.AdministratorActivity;
import com.example.hospital.UI.old.DoctorActivity;
import com.example.hospital.UI.old.PatientActivity;

public class UserRepository {
    private static UserRepository instance = null;
    private MutableLiveData <User> loggedInUser = new MutableLiveData<>();
    private MutableLiveData <Boolean> isLoggingIn = new MutableLiveData<>();

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    private UserRepository() {
        isLoggingIn.setValue(false);
    }

    public LiveData<User> getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User user) {
        loggedInUser.setValue(user);
    }

    public LiveData<Boolean> getLoggingIn() {
        return isLoggingIn;
    }

    public void setLoggingIn(boolean isLoggingIn) {
        this.isLoggingIn.setValue(isLoggingIn);
    }

    public void login(String login, String password) {
        isLoggingIn.setValue(true);

        for (Patient patient : Repo.patients) {
            if (login.equals(patient.getLogin()) && password.equals(patient.getPassword())) {
                loggedInUser.setValue(patient);
            }
        }
        for (Doctor doctor : Repo.doctors) {
            if (login.equals(doctor.getLogin()) && password.equals(doctor.getPassword())) {
                loggedInUser.setValue(doctor);
            }
        }
        for (Administrator admin : Repo.admins) {
            if (login.equals(admin.getLogin()) && password.equals(admin.getPassword())) {
                loggedInUser.setValue(admin);
            }
        }
        isLoggingIn.setValue(false);
    }
}
