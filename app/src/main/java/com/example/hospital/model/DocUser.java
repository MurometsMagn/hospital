package com.example.hospital.model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class DocUser { //джойн трех таблиц
    @Embedded
    public User user;
    @Relation(
            entity = Doc.class,
            parentColumn = "ID",
            entityColumn = "userId"
    )
    public DocSpeciality docSpeciality; //джойн двух таблиц
//    @Relation(
//            parentColumn = "id",
//            entityColumn = "specialityId"
//    )
//    public Speciality speciality;
}
