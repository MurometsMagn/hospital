package com.example.hospital.model;

import android.util.Log;

import com.example.hospital.model.Doctor.Speciality;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Repo { //сделать синглетоном?
    public static final List<Administrator> admins = new ArrayList<>();
    public static final List<Doctor> doctors = new ArrayList<>();
    public static final List<Patient> patients = new ArrayList<>();
    public static final List<Boolean> doctorsSelected = new ArrayList<>();


    public static List<Doctor> getDoctorBySpeciality(Speciality speciality) {
        List<Doctor> especialDoctor = new ArrayList<>();
        for (Doctor doctor : doctors) {
            //if (doctor.getSpeciality().compareTo(speciality) == 0) {//лучше compareTo чем equals
            if (doctor.getSpeciality() == speciality) { //т.к. ENUM
                especialDoctor.add(doctor);
            }
        }
        return especialDoctor;
    }

    /*static {//инициализатор класса
        admins.add(new Administrator(1,"1", "1", "Il'ya", "Nickolaevitch",
                "Smolentsev"));
        doctors.add(new Doctor(1,"2", "2", "Vasya", "Vasolevitch",
                "Vasiliev", Doctor.Speciality.SURGEON));
        doctors.add(new Doctor(2,"4", "4", "айболит", "Vasilevitch",
                "Koliev", Doctor.Speciality.SURGEON));
        patients.add(new Patient(1, "3", "3", "Anton", "Petrovich",
                "Ulyanov"));

        for (int i = 0; i < doctors.size(); i++) {
            doctorsSelected.add(false);
        }
    }*/

    public static void save(FileOutputStream outputStream) throws JSONException, IOException {
        JSONObject root = new JSONObject();

        JSONArray adminsArr = new JSONArray();
        JSONArray doctorsArr = new JSONArray();
        JSONArray patientsArr = new JSONArray();

        for (int i = 0; i < admins.size(); i++) {
            adminsArr.put(admins.get(i).toJSON());
        }
        for (int i = 0; i < doctors.size(); i++) {
            doctorsArr.put(doctors.get(i).toJSON());
        }
        for (int i = 0; i < patients.size(); i++) {
            patientsArr.put(patients.get(i).toJSON());
        }

        root.put("admins", adminsArr);
        root.put("doctors", doctorsArr);
        root.put("patients", patientsArr);

        String json = root.toString();

        Log.d("Hospital", json);

        outputStream.write(json.getBytes());
    }

    public static void load(String nameJSON)
            throws JSONException, TimeTakenException, ParseException {
        Log.d("Hospital", nameJSON);

        JSONObject root = new JSONObject(nameJSON);
        JSONArray adminsArr = root.getJSONArray("admins");
        JSONArray doctorsArr = root.getJSONArray("doctors");
        JSONArray patientsArr = root.getJSONArray("patients");

        for (int i = 0; i < adminsArr.length(); i++) {
            admins.add(Administrator.fromJSON(adminsArr.getJSONObject(i)));
        }

        for (int i = 0; i < doctorsArr.length(); i++) {
            doctors.add(Doctor.fromJSON(doctorsArr.getJSONObject(i)));
            doctorsSelected.add(false);
        }

        for (int i = 0; i < patientsArr.length(); i++) {
            patients.add(Patient.fromJSON(patientsArr.getJSONObject(i)));
        }

        for (int i = 0; i < doctorsArr.length(); i++) {
            doctors.get(i).loadAppointmentFromJSON(doctorsArr.getJSONObject(i));
        }
    }
}

/*
дописать сохранение пациента (назначение, рецепты, комментарии)
дописать сохранение врача (поля)
дописать загрузку пациента и врача
 */