package com.example.hospital.model;

//Врачи принимают пац-в по сфоримированной очереди, составляют диагноз, выписывают назначение и рецепты.

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Doctor extends User {
    private final List<Appointment> appointments = new ArrayList<>();
    private Speciality speciality;

    public Doctor(int ID, String login, String password, String firstName, String middleName,
                  String lastName, Speciality speciality) {
        super(login, password, firstName, middleName, lastName);
        this.speciality = speciality;
    }

    public enum Speciality {
        OFTALMOLOG ,
        OTOLARINGOLOGIST,
        THERAPIST,
        SURGEON,
        NUTRITIONIST;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public Appointment getAppontment(int index) {
        return appointments.get(index);
    }

    public int appointmentsQuantity() {
        return appointments.size();
    }

    public void addAppointment(Appointment appointment) throws TimeTakenException {
        for (int i = 0; i < appointments.size(); i++) {
            Appointment val = appointments.get(i);
            if (val.date.compareTo(appointment.date) == 0) {
                throw new TimeTakenException();
            }
            if (val.date.compareTo(appointment.date) > 0) {
                appointments.add(i, appointment);
                return;
            }
        }
        appointments.add(appointment);
    }

    public void removeAppointment(Appointment appointment) {
        appointments.remove(appointment);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("speciality", speciality.toString());
        JSONArray appointmentsJSON = new JSONArray();

        for (int j = 0; j < appointmentsQuantity(); j++) {
            appointmentsJSON.put(getAppontment(j).toJSON());
        }
        jsonObject.put("appointments", appointmentsJSON);

        return jsonObject;
    }

    public static Doctor fromJSON(JSONObject jsonObject)
            throws JSONException {
        int id = jsonObject.getInt("id");
        String login = jsonObject.getString("login");
        String password = jsonObject.getString("password");
        String firstName = jsonObject.getString("firstName");
        String middleName = jsonObject.getString("middleName");
        String lastName = jsonObject.getString("lastName");
        Speciality speciality = Speciality.valueOf(jsonObject.getString("speciality"));

        Doctor doc = new Doctor(id, login, password, firstName, middleName,
                lastName, speciality);

        return doc;
    }

    public void loadAppointmentFromJSON(JSONObject jsonObject)
            throws JSONException, ParseException, TimeTakenException {
        JSONArray appointmentsJSON = jsonObject.getJSONArray("appointments");
        for (int j = 0; j < appointmentsJSON.length(); j++) {
            addAppointment(Appointment.fromJSON(appointmentsJSON.getJSONObject(j)));
        }
    }
}

/*25.03
отладить запись пациента к врачу
возм: сохранение и загрузка json
        не вызывается ф-я сох-я загрузки
        данные теряются после загрузки
        алгоритм поиска сл пациента
 */