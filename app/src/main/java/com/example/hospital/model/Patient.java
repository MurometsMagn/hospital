package com.example.hospital.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Patient extends User {
    private final List<String> diagnoses = new ArrayList<>();
    private final List<String> prescriptions = new ArrayList<>(); //назначение
    private final List<String> prescripts = new ArrayList<>(); //рецепт
    private final List<Comment> comments = new ArrayList<>();

    public Patient(int ID, String login, String password, String firstName, String middleName, String lastName) {
        super(login, password, firstName, middleName, lastName);
    }

    //val //kotlin
    //val diagnoses = mutableListOf<String>()
    //MutableList<String> //сгенерированный тип данных
    //все, что можно - делать val. var - опасен


    public String getDiagnose(int index) {
        return diagnoses.get(index);
    }

    public String getDiagnoseString() { //все диагнозы через запятую
        if (diagnoses.size() == 0) return "";

        StringBuilder sb = new StringBuilder(diagnoses.get(0));

        for (int i = 1; i < diagnoses.size(); i++) {
            sb.append(", ");
            sb.append(diagnoses.get(i));
        }
        return sb.toString();
    }

    public int diagnosesQuantity() {
        return diagnoses.size();
    }

    public void addDiagnose(String diagnose) {
        diagnoses.add(diagnose);
    }

    public String getPrescription(int index) {
        return prescriptions.get(index);
    }

    public int prescriptionsQuantity() {
        return prescriptions.size();
    }

    public void addPrescription(String prescription) {
        prescriptions.add(prescription);
    }

    public String getPrescript(int index) {
        return prescripts.get(index);
    }

    public int prescriptsQuantity() {
        return prescripts.size();
    }

    public void addPrescript(String prescript) {
        prescripts.add(prescript);
    }

    public Comment getComment(int index) {
        return comments.get(index);
    }

    public int commentsQuantity() {
        return comments.size();
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public List<String> getDiagnoses() {
        return diagnoses;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();

        jsonObject.put("diagnoses", fieldsToJSON(diagnoses));
        jsonObject.put("prescriptions", fieldsToJSON(prescriptions));
        jsonObject.put("prescripts", fieldsToJSON(prescripts));

       /* JSONArray diagnosesJSON = new JSONArray();
        for (int j = 0; j < diagnosesQuantity(); j++) {
            diagnosesJSON.put(getDiagnose(j));
        }
        jsonObject.put("diagnoses", diagnosesJSON);

        JSONArray prescriptionsJSON = new JSONArray(); //попробовать вынести в однотипный метод
        for (int j = 0; j < prescriptionsQuantity(); j++) {
            prescriptionsJSON.put(getPrescription(j));
        }
        jsonObject.put("prescriptions", prescriptionsJSON);

        JSONArray prescriptsJSON = new JSONArray();
        for (int j = 0; j < prescriptsQuantity(); j++) {
            prescriptsJSON.put(getPrescript(j));
        }
        jsonObject.put("prescripts", prescriptsJSON);
*/
        JSONArray commentsJSON = new JSONArray();
        for (int j = 0; j < commentsQuantity(); j++) {
            commentsJSON.put(getComment(j).toJSON());
        }
        jsonObject.put("comments", commentsJSON);

        return jsonObject;
    }

    public static Patient fromJSON(JSONObject jsonObject)
            throws JSONException, ParseException, TimeTakenException {
        int id = jsonObject.getInt("id");
        String login = jsonObject.getString("login");
        String password = jsonObject.getString("password");
        String firstName = jsonObject.getString("firstName");
        String middleName = jsonObject.getString("middleName");
        String lastName = jsonObject.getString("lastName");

        Patient patient = new Patient(id, login, password, firstName, middleName,
                lastName);

        JSONArray diagnosesJSON = jsonObject.getJSONArray("diagnoses");
        for (int j = 0; j < diagnosesJSON.length(); j++) {
            patient.addDiagnose(diagnosesJSON.getString(j));
        }

        JSONArray prescriptionsJSON = jsonObject.getJSONArray("prescriptions");
        for (int j = 0; j < prescriptionsJSON.length(); j++) {
            patient.addPrescription(prescriptionsJSON.getString(j));
        }

        JSONArray prescriptsJSON = jsonObject.getJSONArray("prescripts");
        for (int j = 0; j < prescriptsJSON.length(); j++) {
            patient.addPrescript(prescriptsJSON.getString(j));
        }

        JSONArray commentsJSON = jsonObject.getJSONArray("comments");
        for (int j = 0; j < commentsJSON.length(); j++) {
            patient.addComment(Comment.fromJSON(commentsJSON.getJSONObject(j)));
        }

        return patient;
    }

    private JSONArray fieldsToJSON(List<String> field) {
        JSONArray fieldJSON = new JSONArray();
        for (int i = 0; i < field.size(); i++) {
            fieldJSON.put(field.get(i));
        }
        return fieldJSON;
    }
}
