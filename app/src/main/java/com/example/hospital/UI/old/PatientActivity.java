package com.example.hospital.UI.old;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hospital.R;
import com.example.hospital.UI.adapters.DiagnoseAdapter;
import com.example.hospital.UI.adapters.PrescriptAdapter;
import com.example.hospital.UI.adapters.PrescriptionAdapter;
import com.example.hospital.model.Patient;
import com.example.hospital.model.Repo;

public class PatientActivity extends AppCompatActivity {
    private Patient patient;
    private DiagnoseAdapter diagnoseAdapter;
    private PrescriptAdapter prescriptAdapter;
    private PrescriptionAdapter prescriptionAdapter;
    private ListView lvDiagnoses;
    private ListView lvPrescripts;
    private ListView lvPrescriptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        Intent intent = getIntent();
        int ID = intent.getIntExtra("ID", -1);
        if (ID == -1) {
            finish();
            return;
        }
        patient = Repo.patients.get(ID - 1);
        final TextView patientInfo = findViewById(R.id.PatientInfo);
        patientInfo.setText("Пациент: "
                + patient.getLastName() + " " + patient.getFirstName()
                + " " + patient.getMiddleName());

        lvDiagnoses = findViewById(R.id.patDiagnList);
        lvPrescripts = findViewById(R.id.patPrescriptsList);
        lvPrescriptions = findViewById(R.id.patPrescriptionsList);

        diagnoseAdapter = new DiagnoseAdapter(this, patient);
        prescriptAdapter = new PrescriptAdapter(this, patient);
        prescriptionAdapter = new PrescriptionAdapter(this, patient);

        lvDiagnoses.setAdapter(diagnoseAdapter);
        lvPrescriptions.setAdapter(prescriptionAdapter);
        lvPrescripts.setAdapter(prescriptAdapter);

        Button btn = findViewById(R.id.patAssignBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(
                        PatientActivity.this,
                        AssignActivity.class
                );
                intent1.putExtra(
                        "patientInfo", patientInfo.getText().toString()
                );
                startActivity(intent1);
            }
        });
    }
}