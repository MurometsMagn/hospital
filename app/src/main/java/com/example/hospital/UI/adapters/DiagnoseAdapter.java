package com.example.hospital.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.hospital.model.Patient;

public class DiagnoseAdapter extends BaseAdapter {
    private Context context;
    private Patient patient;

    public DiagnoseAdapter(Context context, @Nullable Patient patient) {
        this.context = context;
        this.patient = patient;
    }

    public void setPatient(@Nullable Patient patient) {
        this.patient = patient;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (patient == null) return 0;
        return patient.diagnosesQuantity();
    }

    @Override
    public Object getItem(int position) {
        if (patient == null) return null;
        return patient.getDiagnose(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (patient == null) return null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(android.R.layout.simple_list_item_1, null);
        }
        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(patient.getDiagnose(position));

        return convertView;
    }
}

/**
 * 16.03
 * дописать прескриптАдаптер и прескрипшинАдаптер проверками пациентов на нал.
 * в докторАктивити связать листВью и адаптеры
 */
