package com.example.hospital.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.hospital.R;
import com.example.hospital.model.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorsListAdapter extends ArrayAdapter<Doctor> { //типовой шаблон создания адаптера
    private final List<Doctor> doctors;
    private final List<Boolean> doctorsSelected;

    public DoctorsListAdapter(Context context, List<Doctor> doctors,
                              @Nullable List<Boolean> doctorsSelected) {
        super(context, R.layout.list_doctor_selectable, doctors);
        this.doctors = doctors;
        this.doctorsSelected = doctorsSelected;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView,
                        @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_doctor_selectable, null);
        }
        TextView textView = convertView.findViewById(R.id.doctorName);
        //TextView textView2 = convertView.findViewById(android.R.id.text2);
        Doctor doctor = doctors.get(position);
        textView.setText(doctor.getLastName() + " " +
                doctor.getFirstName() + " " + doctor.getMiddleName());
        //textView2.setText("Врач");

        CheckBox selectionCheckBox = convertView.findViewById(R.id.DoctorSelected);
        if (doctorsSelected == null) {
            selectionCheckBox.setVisibility(View.GONE);
        } else {
            selectionCheckBox.setChecked(doctorsSelected.get(position)); //состояние чекбокса
            selectionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    for (int i = 0; b && i < doctorsSelected.size(); i++) {
                        doctorsSelected.set(i, false);
                    }
                    doctorsSelected.set(position, b);
                    notifyDataSetChanged(); //метод адаптера: иниц перерисовка данных в представлении
                }
            });
            selectionCheckBox.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}

/*
2.03.21
отс-ет обработка чекбокса
получить чекбокс, навесить обработчик, добавить массив булевых значений (в параметры констр-ра и в поле)
добавить его в обработчик чекбокса,
 */

/*
1. в эссайнАктивити определить каких врачей выбрали
2. получить дату и время для записи
3. записать пациента врачам на заданное время
4. проверить что выбран только один врач и выбран ли вообще
5.сообщение об ошибке выдать Тостом
 */