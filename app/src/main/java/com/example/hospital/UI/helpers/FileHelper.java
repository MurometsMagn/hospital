package com.example.hospital.UI.helpers;

import android.content.Context;
import android.widget.Toast;

import com.example.hospital.model.Repo;
import com.example.hospital.model.TimeTakenException;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class FileHelper {
    private static final String fileName = "users.JSON";

    public static void load(Context context) {
        try {
            Repo.load(readFileInternalStorage(context));
        } catch (JSONException e) {
            Toast.makeText(context, "Поврежденный файл сохранения, либо неверный формат",
                    Toast.LENGTH_LONG)
                    .show();
        } catch (IOException e) {
            Toast.makeText(context, "Ошибка ввода-вывода",
                    Toast.LENGTH_LONG)
                    .show();
        } catch (ParseException e) {
            Toast.makeText(context, "Ошибка формата даты и времени",
                    Toast.LENGTH_LONG)
                    .show();
        } catch (TimeTakenException e) {
            Toast.makeText(context, "Конфликт времени записи в конфигурационном файле",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    public static void save(Context context) {
        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            Repo.save(outputStream);//outputStream.write(content.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            Toast.makeText(context, "Ошибка ввода-вывода",
                    Toast.LENGTH_LONG)
                    .show();
        } catch (JSONException e) {
            Toast.makeText(context, "Поврежденный файл сохранения, либо неверный формат",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    private static String readFileInternalStorage(Context context) throws IOException {
        FileInputStream fileInputStream = context.openFileInput(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));

        StringBuffer sb = new StringBuffer();
        String line = reader.readLine();

        while (line != null) {
            sb.append(line);
            line = reader.readLine();
        }
        return sb.toString();
    }
}

/*30.03.2021
рекомендации по интерфейсу программы.
У каждого пользователя активити из шаблона TabbedActivity
и основное содержимое содержит либо не более одного элемента управления,
либо не более одной формы.
Добавление комментариев, диагнозов, назначений, рецептов, записей  к врачу
выполняется на отдельной активити по шаблону EmptyActivity (с экшнБар).
ЛогинАктивити по шаблону LoginActivity.
Действия, выполняемые на страницах пользователей, разместить либо
в виде иконки в экшнБар, либо в виде меню по трем точкам
 */