package com.example.hospital.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hospital.model.Comment;
import com.example.hospital.model.Patient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class CommentsAdapter extends BaseAdapter {
    private Context context;
    private Patient patient;

    public CommentsAdapter(Context context, Patient patient) {
        this.context = context;
        this.patient = patient;
    }

    @Override
    public int getCount() {
        return patient.commentsQuantity();
    }

    @Override
    public Object getItem(int i) {
        return patient.getComment(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(android.R.layout.simple_list_item_2, null);
        }
        TextView tvAuthor = convertView.findViewById(android.R.id.text1);
        TextView tvComment = convertView.findViewById(android.R.id.text2);
        Comment comment = patient.getComment(position);
        String docName = comment.getAuthor().getFullName();
        Date date = comment.getDate();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        String strDate = dateFormat.format(date);

        String commentHeader = docName + " (" + strDate + ")";
        tvAuthor.setText(commentHeader);

        tvComment.setText(comment.getMessage());

        return convertView;
    }
}
