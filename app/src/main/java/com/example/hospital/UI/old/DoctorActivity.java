package com.example.hospital.UI.old;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hospital.R;
import com.example.hospital.UI.adapters.DiagnoseAdapter;
import com.example.hospital.UI.adapters.PrescriptAdapter;
import com.example.hospital.UI.adapters.PrescriptionAdapter;
import com.example.hospital.UI.helpers.FileHelper;
import com.example.hospital.model.Appointment;
import com.example.hospital.model.Doctor;
import com.example.hospital.model.Patient;
import com.example.hospital.model.Repo;

import java.util.Date;

public class DoctorActivity extends AppCompatActivity {
    private Doctor doctor;
    private Patient currentPatient = null;
    private TextView tvPatientName;
    private ListView lvDiagnoses;
    private ListView lvPrescriptions;
    private ListView lvPrescripts;
    private DiagnoseAdapter diagnoseAdapter;
    private PrescriptAdapter prescriptAdapter;
    private PrescriptionAdapter prescriptionAdapter;
    private String inputBoxResult = "";
    private boolean inputBoxSuccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        final Intent intent = getIntent();//c помощью которого эта активити была вызвана
        int ID = intent.getIntExtra("ID", -1);
        if (ID == -1) {
            finish(); //завершает текущее активити
            return;   //завершает онкриэйт
        }
        doctor = Repo.doctors.get(ID - 1);

        getNextPatient();

        TextView tvDocName = findViewById(R.id.docInfo);
        tvDocName.setText(doctor.getFullName());

        tvPatientName = findViewById(R.id.patientName);
        if (currentPatient != null) {
            tvPatientName.setText(currentPatient.getFullName());
        } else {
            tvPatientName.setText("Нет ближайшего пациента");
        }

        lvDiagnoses = findViewById(R.id.diagnoses);
        lvPrescriptions = findViewById(R.id.prescriptions);
        lvPrescripts = findViewById(R.id.prescripts);

        diagnoseAdapter = new DiagnoseAdapter(this, currentPatient);
        prescriptionAdapter = new PrescriptionAdapter(this, currentPatient);
        prescriptAdapter = new PrescriptAdapter(this, currentPatient);

        lvDiagnoses.setAdapter(diagnoseAdapter);
        lvPrescriptions.setAdapter(prescriptionAdapter);
        lvPrescripts.setAdapter(prescriptAdapter);

        Button nextPatBtn = findViewById(R.id.nextPatBtn);
        nextPatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNextPatient();
                diagnoseAdapter.setPatient(currentPatient);
                prescriptionAdapter.setPatient(currentPatient);
                prescriptAdapter.setPatient(currentPatient);
            }
        });

        Button addDiagnoseBtn = findViewById(R.id.addDiagnoseBtn);
        Button addPrescriptionBtn = findViewById(R.id.addPrescriptionBtn);
        Button addPrescriptBtn = findViewById(R.id.addPrescriptBtn);
        Button addCommentBtn = findViewById(R.id.addComBtn);

        addDiagnoseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPatient != null) {
                    showInputBox("Новый диагноз");
                    if (inputBoxSuccess) {
                        currentPatient.addDiagnose(inputBoxResult);
                        diagnoseAdapter.notifyDataSetChanged();
                        FileHelper.save(DoctorActivity.this);
                    }
                }
            }
        });

        addPrescriptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPatient != null) {
                    showInputBox("Новое назначение");
                    if (inputBoxSuccess) {
                        currentPatient.addPrescription(inputBoxResult);
                        prescriptionAdapter.notifyDataSetChanged();
                        FileHelper.save(DoctorActivity.this);
                    }
                }
            }
        });

        addPrescriptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPatient != null) {
                    showInputBox("Новый рецепт");
                    if (inputBoxSuccess) {
                        currentPatient.addPrescript(inputBoxResult);
                        prescriptAdapter.notifyDataSetChanged();
                        FileHelper.save(DoctorActivity.this);
                    }
                }
            }
        });

        addCommentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPatient != null) {
                    Intent intent1 = new Intent(DoctorActivity.this,
                            CommentActivity.class);
                    intent1.putExtra("DoctorID", doctor.getID());
                    intent1.putExtra("PatientID", currentPatient.getID());
                    startActivity(intent1);
                }
            }
        });
    }

    private void getNextPatient() {
        Date now = new Date(); // new Date(без параметров) = текущее время
        long difference = Long.MAX_VALUE;
        for (int i = 0; i < doctor.appointmentsQuantity(); i++) {
            Appointment appointment = doctor.getAppontment(i);
            long currentDif = appointment.date.getTime() - now.getTime();
            if (currentDif < difference && currentDif >= 0) {
                difference = currentDif;
                currentPatient = appointment.patient;
            }
        }
    }

    private void showInputBox(String title) {
        //AlertDialog - для андроида делает всплывающие окна
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                inputBoxResult = input.getText().toString();
                inputBoxSuccess = true;
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                inputBoxSuccess = false;
                dialog.cancel();
            }
        });

        builder.show();
    }
}

/*18.03
cоздать активити и кнопку перехода на нее
активити коментарии врачей к пациентам
кнопка должна находиться на докторАктивити

активити для коментов должна содержать:
список, каждая строка которого это имя автора комента и текст комента,
Поле для ввода нового коммента,
кнопка создающая коммент.
 */