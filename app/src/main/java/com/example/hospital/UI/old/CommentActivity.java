package com.example.hospital.UI.old;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.hospital.R;
import com.example.hospital.UI.adapters.CommentsAdapter;
import com.example.hospital.UI.helpers.FileHelper;
import com.example.hospital.model.Comment;
import com.example.hospital.model.Doctor;
import com.example.hospital.model.Patient;
import com.example.hospital.model.Repo;

import java.util.Date;

public class CommentActivity extends AppCompatActivity {
    private Doctor doctor;
    private Patient patient;
    private ListView lvComments;
    private CommentsAdapter commentsAdapter;
    private EditText etComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Intent intent = getIntent();
        int doctorID = intent.getIntExtra("DoctorID", -1);
        int patientID = intent.getIntExtra("PatientID", -1);
        if (doctorID == -1 || patientID == -1) {
            finish();
            return;
        }
        doctor = Repo.doctors.get(doctorID - 1);
        patient = Repo.patients.get(patientID - 1);

        lvComments = findViewById(R.id.comments_list);
        commentsAdapter = new CommentsAdapter(this, patient);
        lvComments.setAdapter(commentsAdapter);

        etComment = findViewById(R.id.commentInput);

        Button commentBtn = findViewById(R.id.commentBtn);

        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = etComment.getText().toString();
                if(text.isEmpty()) return;
                Comment comment = new Comment(doctor, new Date(), text);
                patient.addComment(comment);
                commentsAdapter.notifyDataSetChanged();

                FileHelper.save(CommentActivity.this);
            }
        });
    }
}

/* 23.03.21
по нажатию на кнопку добавления комментария добавить коммент к пациенту
 */