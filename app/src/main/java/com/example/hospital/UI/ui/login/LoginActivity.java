package com.example.hospital.UI.ui.login;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.hospital.UI.R;
import com.example.hospital.R;
import com.example.hospital.UI.ui.login.LoginViewModel;
import com.example.hospital.UI.ui.login.LoginViewModelFactory;
import com.example.hospital.model.Admin;
import com.example.hospital.model.AdminUser;
import com.example.hospital.model.AppDatabase;
import com.example.hospital.model.Doc;
import com.example.hospital.model.DocUser;
import com.example.hospital.model.Pat;
import com.example.hospital.model.PatUser;
import com.example.hospital.model.User;
import com.example.hospital.model.UserDao;
import com.example.hospital.model.UserRepository;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private UserRepository userRepository;

    //private LoginViewModel loginViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "students.db").build();

    //Room.inMemoryDatabaseBuilder(this, AppDatabase.class); // база данных в оперативной памяти :memory:

        final Admin admin = new Admin();
        final Doc doctor = new Doc();
        final Pat patient = new Pat();
        admin.userId = 1;
        doctor.userId = 2;
        patient.userId = 3;

        final UserDao ud = db.userDao();

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10_000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ud.add(doctor);
                Log.d("HOSPITAL", "DoctorAdded!"); //логкат вместо терминальной строки
            }
        }).start();*/

        ud.getAllDoctors().observe(this, new Observer<List<DocUser>>() {
            @Override
            public void onChanged(List<DocUser> docUsers) {
                Log.d("HOSPITAL", "SomeText");
                for (DocUser docUser : docUsers) {
                    Log.d("HOSPITAL",
                            docUser.user.ID + " " + docUser.user.login + " " +
                                    docUser.user.lastName + " " + docUser.docSpeciality.doctor.id);
                }
            } // alt+J choose next, F3 skip
        });

        /** 20.04.2021
         * В ЮзерДао добавить методы получения всех докторов и пациентов (два отд. метода),
         * В ЛогинАктивити протестировать добавление доктора и пациента, убедиться
         * в том, что они добавлены. при помощи метода, который мы добавили в первом шаге.
         */




        userRepository = UserRepository.getInstance();
        //loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
        //       .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        userRepository.getLoggingIn().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                int visibility;
                if (aBoolean) {
                    visibility = View.VISIBLE;
                } else {
                    visibility = View.GONE;
                }

                loadingProgressBar.setVisibility(visibility);
            }
        });

        userRepository.getLoggedInUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                finish();
            }
        });



//        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
//            @Override
//            public void onChanged(@Nullable LoginFormState loginFormState) {
//                if (loginFormState == null) {
//                    return;
//                }
//                loginButton.setEnabled(loginFormState.isDataValid());
//                if (loginFormState.getUsernameError() != null) {
//                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
//                }
//                if (loginFormState.getPasswordError() != null) {
//                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
//                }
//            }
//        });
//
//        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
//            @Override
//            public void onChanged(@Nullable LoginResult loginResult) {
//                if (loginResult == null) {
//                    return;
//                }
//                loadingProgressBar.setVisibility(View.GONE);
//                if (loginResult.getError() != null) {
//                    showLoginFailed(loginResult.getError());
//                }
//                if (loginResult.getSuccess() != null) {
//                    updateUiWithUser(loginResult.getSuccess());
//                }
//                setResult(Activity.RESULT_OK);
//
//                //Complete and destroy login activity once successful
//                finish();
//            }
//        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                //loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                //       passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    loginViewModel.login(usernameEditText.getText().toString(),
//                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userRepository.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());

//                loadingProgressBar.setVisibility(View.VISIBLE);
//                loginViewModel.login(usernameEditText.getText().toString(),
//                        passwordEditText.getText().toString());
            }
        });
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}