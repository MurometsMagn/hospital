package com.example.hospital.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.hospital.model.Patient;

public class PrescriptionAdapter extends BaseAdapter {
    private Context context;
    private Patient patient;

    public PrescriptionAdapter(Context context, Patient patient) {
        this.context = context;
        this.patient = patient;
    }

    public void setPatient(@Nullable Patient patient) {
        this.patient = patient;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (patient == null) return 0;
        return patient.prescriptionsQuantity();
    }

    @Override
    public Object getItem(int position) {
        if (patient == null) return null;
        return patient.getPrescription(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (patient == null) return null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(android.R.layout.simple_list_item_1, null);
        }
        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(patient.getPrescription(position));

        return convertView;
    }
}
