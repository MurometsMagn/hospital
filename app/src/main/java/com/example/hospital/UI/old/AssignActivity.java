package com.example.hospital.UI.old;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hospital.R;
import com.example.hospital.UI.adapters.DoctorsListAdapter;
import com.example.hospital.UI.helpers.FileHelper;
import com.example.hospital.model.Appointment;
import com.example.hospital.model.Doctor;
import com.example.hospital.model.Patient;
import com.example.hospital.model.Repo;
import com.example.hospital.model.TimeTakenException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AssignActivity extends AppCompatActivity {
    private DoctorsListAdapter doctorsListAdapter;
    private EditText patientName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign);

        /**
         * 1. создать экз адаптера в онкриэйте
         * 2. получить экз листвью
         * 3. вызвать метод сетадаптер для листвью
         */
        doctorsListAdapter = new DoctorsListAdapter(this, Repo.doctors, Repo.doctorsSelected);
        ListView listView = findViewById(R.id.DoctorsList);
        listView.setAdapter(doctorsListAdapter);

        Intent intent = getIntent();
        String patientInfo = intent.getStringExtra("patientInfo");
        patientName = findViewById(R.id.PatientName);
        patientName.setText(patientInfo);

        Button button = findViewById(R.id.createAssignBtn);

        final EditText dateET = findViewById(R.id.editTextDate);
        final EditText timeET = findViewById(R.id.editTextTime);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String patNameStr = patientName.getText().toString();
                String[] fullName = patNameStr.split(" ", 3);
                Patient selectedPatient = null;
                for (Patient patient : Repo.patients) {
                    if (fullName[0].equals(patient.getLastName()) &&
                            fullName[1].equals(patient.getFirstName()) &&
                            fullName[2].equals(patient.getMiddleName())) {
                        selectedPatient = patient;
                        break;
                    }
                }
                if (selectedPatient == null) {
                    selectedPatient = new Patient(Repo.patients.size() + 1, patNameStr,
                            "1", fullName[1], fullName[2], fullName[0]);
                    Repo.patients.add(selectedPatient);
                }

                String dateSelect = dateET.getText().toString();
                String timeSelect = timeET.getText().toString();
                String dateTimeSel = dateSelect + " " + timeSelect;
                SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy hh:mm");
                Date date;
                try {
                    date = formater.parse(dateTimeSel);
                } catch (ParseException e) {
                    Toast.makeText(AssignActivity.this,
                            "Неверный формат даты или времени",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }

                Doctor selectedDoctor = null;
                for (int i = 0; i < Repo.doctorsSelected.size(); i++) {
                    if (Repo.doctorsSelected.get(i)) {
                        selectedDoctor = Repo.doctors.get(i);
                        break;
                    }
                }

                if (selectedDoctor == null) {
                    Toast.makeText(AssignActivity.this,
                            "Доктор не выбран",
                            Toast.LENGTH_LONG)
                            .show();
                    return; //завершает обработчик нажатия кнопки
                }

                try {
                    selectedDoctor.addAppointment(new Appointment(date, selectedPatient));
                } catch (TimeTakenException e) {
                    Toast.makeText(AssignActivity.this,
                            "Выбранное время занято",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }

                FileHelper.save(AssignActivity.this);
                finish(); //завершение активити и возврат к предыдущей акт-ти
            }
        });
    }
}
/**
 * 9.03
 * добавить поиск выбранного врача,
 * если врач не найден - выдать ошибку тостом
 * если врач найден = добавить ему назначение и завершить активити
 */

/*
2/03/21
отс-ет поиск выбранного врача и добавление ему назначения

сделать возможность автоматически подстановки имени пациента в эссайнАктивити
(если переходим из ПатиентАктивити) putExtra в intent
putExtra - отправить в пацАктивити
в адмАктивити не писать путЭкстра
Получить его в эссайнАктивити
записать в текстбокс
 */