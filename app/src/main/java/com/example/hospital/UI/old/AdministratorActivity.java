package com.example.hospital.UI.old;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.hospital.R;
import com.example.hospital.UI.adapters.DoctorsListAdapter;
import com.example.hospital.UI.adapters.PatientsListAdapter;
import com.example.hospital.model.Administrator;
import com.example.hospital.model.Repo;

public class AdministratorActivity extends AppCompatActivity {
    private Administrator admin;
    private DoctorsListAdapter doctorsListAdapter;
    private PatientsListAdapter patientsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrator);

        Intent intent = getIntent();//c помощью которого эта активити была вызвана
        int ID = intent.getIntExtra("ID", -1);
        if (ID == -1) {
            finish();
            return;
        }
        admin = Repo.admins.get(ID - 1);

        TextView adminInfo = findViewById(R.id.AdminInfo);
        adminInfo.setText("Администратор: "
                + admin.getLastName() + " " + admin.getFirstName()
                + " " + admin.getMiddleName());

        /**
         * 1. создать экз адаптера в онкриэйте
         * 2. получить экз листвью
         * 3. вызвать метод сетадаптер для листвью
         */
        doctorsListAdapter = new DoctorsListAdapter(this, Repo.doctors, null); //можно context Application
        ListView listView = findViewById(R.id.DoctorsList);
        listView.setAdapter(doctorsListAdapter);

        Button signUpBtn = findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent
                        (AdministratorActivity.this, AssignActivity.class);
                //return new Intent(this, PatientActivity.class);
                startActivity(intent1);
            }
        });
    }
}



