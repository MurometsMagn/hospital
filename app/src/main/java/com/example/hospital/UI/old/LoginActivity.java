package com.example.hospital.UI.old;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hospital.R;
import com.example.hospital.UI.helpers.FileHelper;
import com.example.hospital.model.Administrator;
import com.example.hospital.model.Doctor;
import com.example.hospital.model.Patient;
import com.example.hospital.model.Repo;

public class LoginActivity extends AppCompatActivity {
    private Button loginButton;
    private EditText loginField;
    private EditText passwordField;
    private TextView authentReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Log.d("HOSPITAL", "Hello World!"); //логкат вместо терминальной строки

        loginButton = findViewById(R.id.buttonLogin);
        loginField = findViewById(R.id.InputLogin);
        passwordField = findViewById(R.id.InputPassword);
        authentReply = findViewById(R.id.authentReply);

        addListenerOnButton();

        //FileHelper.save(this);
        FileHelper.load(this);
    }

    private void addListenerOnButton() {
        loginButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent;
//                        do {
//                            intent = authentificate(loginField.getText().toString(), passwordField.getText().toString());
//                            authentReply.setText("Authentification failed, try again.");
//                        } while (intent == null);

                        intent = authentificate(loginField.getText().toString(), passwordField.getText().toString());
                        if (intent != null) {
                            startActivity(intent);
                        } else {
                            authentReply.setText("Authentification failed, try again.");
                            authentReply.setVisibility(View.VISIBLE);
                        }

                        //Intent intent = new Intent(".UI.AdministratorActivity"); //расписать выбор активити
                        //дописать иф, разобраться с запуском тлф или эмулятор
                    }
                }
        );
    }

    private Intent authentificate(String login, String password) {
        for (Patient patient : Repo.patients) {
            if (login.equals(patient.getLogin()) && password.equals(patient.getPassword())) {
                //return new Intent(PatientActivity.class.getName());
                Intent intent = new Intent(this, PatientActivity.class);
                intent.putExtra("ID", patient.getID());
                return intent;
            }
        }
        for (Doctor doctor : Repo.doctors) {
            if (login.equals(doctor.getLogin()) && password.equals(doctor.getPassword())) {
                Intent intent = new Intent(this, DoctorActivity.class);
                intent.putExtra("ID", doctor.getID());
                return intent;
            }
        }
        for (Administrator admin : Repo.admins) {
            if (login.equals(admin.getLogin()) && password.equals(admin.getPassword())) {
                Intent intent = new Intent(this, AdministratorActivity.class);
                intent.putExtra("ID", admin.getID());
                return intent;
            }
        }
        return null; //если нет такого логина и пароля
    }
}

/**
 * 11.03
 * 1. в доктор активити получить идентификатор вошедшего врача
 * 2. по идентификатору получить врача и сохранить в поле класса
 * 3. в списке назначений найти то, которое ближе всего по времени к текущему
 * 4. отобразить в графич интерфейсе имя текущего врача и имя пациента из назначения
 */
