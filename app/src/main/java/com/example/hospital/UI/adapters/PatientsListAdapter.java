package com.example.hospital.UI.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.hospital.model.Patient;

import java.util.List;

public class PatientsListAdapter extends ArrayAdapter<Patient> {
    private List<Patient> patients;

    public PatientsListAdapter(Context context, List<Patient> patients) {
        super(context, android.R.layout.simple_list_item_2, patients);
        this.patients = patients;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,
                        @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(android.R.layout.simple_list_item_2, null);
        }
        TextView textView = convertView.findViewById(android.R.id.text1); //fio
        TextView textView2 = convertView.findViewById(android.R.id.text2); //diagnose
        Patient patient = patients.get(position);
        textView.setText(patient.getLastName() + " " +
                patient.getFirstName() + " " + patient.getMiddleName());
        textView2.setText(patient.getDiagnoseString());
        return convertView;

        //return super.getView(position, convertView, parent);
    }
}
